﻿using Beltis.Challenge.ProductScrapping.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.Serialization;

namespace Beltis.Challenge.ProductScrapping.Data.Context
{
    [ExcludeFromCodeCoverage]
    public class ProductScrappingContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        public ProductScrappingContext(DbContextOptions<ProductScrappingContext> options)
        : base(options)
        {
        }   

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema("beltis");

            modelBuilder
              .Entity<Product>()
              .Property(e => e.Status)
              .HasConversion(new EnumToStringConverter<Status>());
        }
    }
}
