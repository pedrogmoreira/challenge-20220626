﻿using Beltis.Challenge.ProductScrapping.Data.Context;
using Beltis.Challenge.ProductScrapping.Data.Repositories;
using Beltis.Challenge.ProductScrapping.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.CodeAnalysis;

namespace Beltis.Challenge.ProductScrapping.Data
{
    [ExcludeFromCodeCoverage]
    public static class Injector
    {
        public static void ConfigureData(this IServiceCollection services, IConfiguration configuration)
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (env == "Test")
            {
                services.ConfigureFake();
            }
            else
            {
                services.Configure(configuration);
            }
        }

        private static void Configure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ProductScrappingContext>(options =>
              options.UseSqlServer(configuration.GetConnectionString("SqlServer")));
            
            services.ConfigureDI();
        }

        private static void ConfigureFake(this IServiceCollection services)
        {
            services.AddDbContext<ProductScrappingContext>(options =>
              options.UseInMemoryDatabase("ProductScrapping.Test.Api"));

            services.ConfigureDI();
        }

        private static void ConfigureDI(this IServiceCollection services)
        {
            services.AddScoped<IProductRepository, ProductRepository>();
        }        
    }
}
