﻿using Beltis.Challenge.ProductScrapping.Data.Context;
using Beltis.Challenge.ProductScrapping.Domain.Entities;
using Beltis.Challenge.ProductScrapping.Domain.Repositories;
using Beltis.Challenge.ProductScrapping.Domain.ResponseObject;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Beltis.Challenge.ProductScrapping.Data.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ProductScrappingContext _context;
        protected readonly DbSet<TEntity> _dbSet;

        protected BaseRepository(ProductScrappingContext database)
        {
            _context = database;
            _dbSet = database.Set<TEntity>();
        }

        /// <summary>
        /// Gets all entities.
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetAll()
        {
            var query = _dbSet.AsNoTracking();

            return query;
        }

        /// <summary>
        /// Gets all entities (paged)
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public virtual PagedQueryResponseObject<TEntity> GetPaged(int page, int pageSize)
        {
            if (page <= 0) return PagedQueryResponseObject<TEntity>.Empty();


            var query = GetAll()
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var totalPages = (int)Math.Ceiling((decimal)GetAll().Count() / pageSize);
            var response = new PagedQueryResponseObject<TEntity>
            {
                Data = query,
                TotalPages = totalPages,
                NextPage = page == totalPages ? page: page + 1,
                PreviousPage = page > 1 ? page - 1 : 1
            };

            return response;
        }

        /// <summary>
        /// Finds an entity with the given primary key value.
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public virtual async Task<TEntity?> Find(Expression<Func<TEntity, bool>> predicate)
        {
            var query = GetAll();
            return await query.Where(predicate).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Inserts a new entity asynchronously. 
        /// </summary>
        /// <param name="entity">The entity to insert.</param>
        public virtual async Task Insert(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Inserts a list of new entities asynchronously. 
        /// </summary>
        /// <param name="entity">The entity list to insert.</param>
        public virtual async Task Insert(IEnumerable<TEntity> entities)
        {
            await _dbSet.AddRangeAsync(entities);
            await _context.SaveChangesAsync();
        }
    }
}
