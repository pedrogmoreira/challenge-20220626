﻿using Beltis.Challenge.ProductScrapping.Data.Context;
using Beltis.Challenge.ProductScrapping.Domain.Entities;
using Beltis.Challenge.ProductScrapping.Domain.Repositories;

namespace Beltis.Challenge.ProductScrapping.Data.Repositories
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(ProductScrappingContext _context) 
            : base(_context)
        {
        }
    }
}
