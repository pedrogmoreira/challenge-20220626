using Beltis.Challenge.ProductScrapping.Business.CronJob;
using Beltis.Challenge.ProductScrapping.Business.Services;
using Beltis.Challenge.ProductScrapping.Domain.Services;
using EasyCronJob.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.CodeAnalysis;

namespace Beltis.Challenge.ProductScrapping.Business
{
    [ExcludeFromCodeCoverage]
    public static class Injector
    {
        public static void ConfigureBusiness(this IServiceCollection services)
        {
            services.AddScoped<IScrappringService, ScrappringService>();
        }

        public static void ConfigureCronJobs(this IServiceCollection services, IConfiguration configuration)
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (env != "Test")
            {
                services.ApplyResulation<PagerScrapperJob>(options =>
                {
                    options.CronExpression = configuration.GetValue<string>("CronJobSchedule");
                    options.TimeZoneInfo = TimeZoneInfo.Local;
                    options.CronFormat = Cronos.CronFormat.Standard;
                });
            }           
        }
    }
}
