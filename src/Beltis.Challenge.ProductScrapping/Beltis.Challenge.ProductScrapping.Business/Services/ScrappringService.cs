﻿using Beltis.Challenge.ProductScrapping.Domain.DTO;
using Beltis.Challenge.ProductScrapping.Domain.Entities;
using Beltis.Challenge.ProductScrapping.Domain.Repositories;
using Beltis.Challenge.ProductScrapping.Domain.Services;
using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Beltis.Challenge.ProductScrapping.Business.Services
{
    public class ScrappringService : IScrappringService
    {
        private readonly ILogger<ScrappringService> _logger;
        private readonly IProductRepository _productRepository;

        public ScrappringService(IProductRepository productRepository, ILogger<ScrappringService> logger)
        {
            _logger = logger;
            _productRepository = productRepository;
        }

        public async Task ScrapProductsFromOpenFoodFacts()
        {
            _logger.LogInformation("Starting page scrapping...");
            
            var products = await ScrapProductsFromPage();
            await _productRepository.Insert(products);
            
            _logger.LogInformation("Page scrapped successfuly.");
        }

        public virtual async Task<IEnumerable<Product>> ScrapProductsFromPage()
        {
            _logger.LogInformation("Retrieving products from page...");

            var html = await FetchSiteHTML();
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            var productList = htmlDoc.DocumentNode.SelectNodes("//div[@id='search_results']//ul//li");
            var products = new List<Product>();

            foreach (HtmlNode productNode in productList)
            {
                if (products.Count == 100) break;

                var href = productNode
                    .ChildNodes
                    .Where(node => node.Name == "a")
                    .Select(a => a.GetAttributeValue("href", "value"))
                    .Single();
                var code = ExtractProductCodeFromHref(href);
                var product = new Product { Id = Guid.NewGuid(), Code = code, Barcode = $"{code} (EAN / EAN-13)", Url = $"https://world.openfoodfacts.org{href}" };
                products.Add(product);
            }

            _logger.LogInformation("Page HTML fetched successfully.");

            return await FetchProducts(products);
        }

        public virtual async Task<IEnumerable<Product>> FetchProducts(IList<Product> products)
        {
            _logger.LogInformation("Fetching products information...");

            var client = GetHttpClient();
            var codes = string.Join(',', products.Select(x => x.Code));
            int page = 1;

            var url = $"https://world.openfoodfacts.org/api/v2/search.json?code={codes}&fields=code,product_name,quantity,categories,packaging,brands,image_url&page={page}";
            var response = await client.GetStringAsync(url);
            var data = JsonConvert.DeserializeObject<ProductFetchJsonDTO>(response);
            var totalPages =  (int) Math.Ceiling ((decimal)products.Count() / int.Parse(data.PageSize));

            do
            {
                foreach (var item in data.Products)
                {
                    var product = products.Where(x => x.Code == long.Parse(item.Code)).Single();

                    if (product is not null)
                    {
                        product.ImportedT = DateTime.UtcNow;
                        product.Status = Status.IMPORTED;
                        product.ProductName = item.ProductName;
                        product.Quantity = item.Quantity;
                        product.Categories = item.Categories;
                        product.Packaging = item.Packaging;
                        product.Brands = item.Brands;
                        product.ImageUrl = item.ImageUrl;
                    }                    
                }

                url = $"https://world.openfoodfacts.org/api/v2/search.json?code={codes}&fields=code,product_name,quantity,categories,packaging,brands,image_url&page={++page}";
                response = await client.GetStringAsync(url);
                data = JsonConvert.DeserializeObject<ProductFetchJsonDTO>(response);
            } while (page != totalPages + 1);

            _logger.LogInformation("Products information fetched successfully.");

            return products;
        }

        public virtual async Task<string> FetchSiteHTML()
        {
            _logger.LogInformation("Fetching page HTML...");

            var url = "https://world.openfoodfacts.org/";
            var client = GetHttpClient();
            var response = await client.GetStringAsync(url);

            _logger.LogInformation("Page HTML fetched successfully.");

            return response;

        }

        public long ExtractProductCodeFromHref(string href)
        {
            _logger.LogInformation("Extracting product code from href...");

            var hrefElements = href.Split('/');
            return long.Parse(hrefElements[2]);
        }

        public virtual HttpClient GetHttpClient()
        {
            return new HttpClient();
        }
    }
}
