﻿using Beltis.Challenge.ProductScrapping.Domain.Services;
using EasyCronJob.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Diagnostics.CodeAnalysis;

namespace Beltis.Challenge.ProductScrapping.Business.CronJob
{
    [ExcludeFromCodeCoverage]
    public class PagerScrapperJob : CronJobService
    {
        private readonly ILogger<PagerScrapperJob> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public PagerScrapperJob(ICronConfiguration<PagerScrapperJob> cronConfiguration, ILogger<PagerScrapperJob> logger, IServiceScopeFactory serviceScopeFactory)
            : base(cronConfiguration.CronExpression, cronConfiguration.TimeZoneInfo, cronConfiguration.CronFormat)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public override Task DoWork(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Executing Page Scrapper Job");

            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var scrappringService = scope.ServiceProvider.GetService<IScrappringService>();

                scrappringService.ScrapProductsFromOpenFoodFacts();
            }

            return base.DoWork(cancellationToken);
        }
    }
}