using Beltis.Challenge.ProductScrapping.Data;
using Beltis.Challenge.ProductScrapping.Business;
using Beltis.Challenge.ProductScrapping.Data.Context;
using Microsoft.EntityFrameworkCore;
using Beltis.Challenge.ProductScrapping.Domain.Services;
using System.Diagnostics.CodeAnalysis;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.ConfigureData(builder.Configuration);
builder.Services.ConfigureBusiness();
builder.Services.ConfigureCronJobs(builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

    using var scope = app.Services.CreateScope();
    var dataContext = scope.ServiceProvider.GetRequiredService<ProductScrappingContext>();
    dataContext.Database.Migrate();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

[ExcludeFromCodeCoverage]
public partial class Program { }