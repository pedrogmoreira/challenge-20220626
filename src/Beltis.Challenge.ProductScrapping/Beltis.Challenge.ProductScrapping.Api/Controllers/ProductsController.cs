﻿using Beltis.Challenge.ProductScrapping.Domain.Repositories;
using Beltis.Challenge.ProductScrapping.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace Beltis.Challenge.ProductScrapping.Api.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductsController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        /// <summary>
        /// Get the challenge name
        /// </summary>
        /// <returns></returns>
        [Route("/")]
        [HttpGet]
        public IActionResult Root()
        {
            return Ok("Fullstack Challenge 20201026");
        }
        
        /// <summary>
        /// Get a single Product
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("products/{code}")]
        public async Task<IActionResult> GetProduct([FromRoute] long code)
        {
            var product = await _productRepository.Find(p => p.Code == code);

            if (product is null)
            {
                return NotFound();
            }

            return Ok(product);
        }
        /// <summary>
        /// Get products in a paged view
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("products")]
        public IActionResult GetPagedProducts([FromQuery] int page, [FromQuery] int pageSize)
        {
            var productResponseObject = _productRepository.GetPaged(page, pageSize);

            if (productResponseObject.TotalPages == 0)
            {
                return NotFound();
            }

            return Ok(productResponseObject);
        }
    }
}
