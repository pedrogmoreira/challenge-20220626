# Backend Challenge 20220626 

[![pipeline status](https://gitlab.com/pedrogmoreira/challenge-20220626/badges/master/pipeline.svg)](https://gitlab.com/pedrogmoreira/challenge-20220626/-/commits/master)
[![coverage report](https://gitlab.com/pedrogmoreira/challenge-20220626/badges/master/coverage.svg)](https://gitlab.com/pedrogmoreira/challenge-20220626/-/commits/master)

## Introdução

Aqui está o desenvolvimento do projeto. 

## Apresentação

[Gravação de apresentação do projeto](https://www.loom.com/share/41a10ea5339c4cce8df7d0c55677a925)

### Executando com Docker

- Certifique-se que tenha o Docker devidamente instalado
- Execute com o perfil **docker-compose**

### Executando sem docker

- Altere a connection string **SqlServer** no arquivo *~/Beltis.Challenge.ProductScrapping.Api/appsettings.json*
- Execute com o perfil **Beltis.Challenge.ProductScrapping.Api**

## Tecnologias Utilizadas

- .Net Core 6
- DDD
- Dependency Injection
- Repository Pattern
- Entity Framework Core
- xUnit
- Docker
- CICD
