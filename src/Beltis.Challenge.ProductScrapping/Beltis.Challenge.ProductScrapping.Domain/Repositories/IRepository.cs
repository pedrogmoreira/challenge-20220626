﻿using Beltis.Challenge.ProductScrapping.Domain.Entities;
using Beltis.Challenge.ProductScrapping.Domain.ResponseObject;
using System.Linq.Expressions;

namespace Beltis.Challenge.ProductScrapping.Domain.Repositories
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        IQueryable<TEntity> GetAll();
        PagedQueryResponseObject<TEntity> GetPaged(int page, int pageSize);
        Task<TEntity?> Find(Expression<Func<TEntity, bool>> predicate);
        Task Insert(TEntity entity);
        Task Insert(IEnumerable<TEntity> entities);
    }
}
