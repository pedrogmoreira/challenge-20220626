﻿using Beltis.Challenge.ProductScrapping.Domain.Entities;

namespace Beltis.Challenge.ProductScrapping.Domain.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
