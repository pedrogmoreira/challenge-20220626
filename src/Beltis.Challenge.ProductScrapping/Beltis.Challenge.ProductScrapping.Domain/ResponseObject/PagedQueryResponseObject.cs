﻿using Beltis.Challenge.ProductScrapping.Domain.Entities;

namespace Beltis.Challenge.ProductScrapping.Domain.ResponseObject
{
    public class PagedQueryResponseObject<TEntity> where TEntity : BaseEntity
    {
        public IEnumerable<TEntity>? Data { get; set; }
        public int TotalPages { get; set; }
        public int PreviousPage { get; set; }
        public int NextPage { get; set; }

        public static PagedQueryResponseObject<TEntity> Empty()
        {
            return new PagedQueryResponseObject<TEntity>
            {
                Data = Enumerable.Empty<TEntity>(),
                TotalPages = 0,
                NextPage = 0,
                PreviousPage = 0
            };
        }
    }
}
