﻿using Beltis.Challenge.ProductScrapping.Domain.Entities;

namespace Beltis.Challenge.ProductScrapping.Domain.Services
{
    public interface IProductService
    {
        Task<Product> GetProduct(long barcode);
        IEnumerable<Product> GetProducts(int page, int pageSize);
    }
}
