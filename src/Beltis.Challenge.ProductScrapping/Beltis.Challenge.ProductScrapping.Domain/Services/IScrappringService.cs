﻿namespace Beltis.Challenge.ProductScrapping.Domain.Services
{
    public interface IScrappringService
    {
        Task ScrapProductsFromOpenFoodFacts();
    }
}
