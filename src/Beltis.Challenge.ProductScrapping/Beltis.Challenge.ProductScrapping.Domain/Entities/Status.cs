﻿using Newtonsoft.Json.Converters;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Beltis.Challenge.ProductScrapping.Domain.Entities
{
    [Newtonsoft.Json.JsonConverter(typeof(StringEnumConverter))]
    public enum Status
    {
        [EnumMember(Value = "IMPORTED")]
        IMPORTED,

        [EnumMember(Value = "DRAFT")]
        DRAFT
    }
}