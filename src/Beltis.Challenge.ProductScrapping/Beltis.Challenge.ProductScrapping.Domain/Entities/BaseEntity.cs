﻿using System.ComponentModel.DataAnnotations;

namespace Beltis.Challenge.ProductScrapping.Domain.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
