﻿using Newtonsoft.Json;

namespace Beltis.Challenge.ProductScrapping.Domain.DTO
{
    public class ProductFetchJsonDTO
    {
        [JsonProperty(PropertyName = "page_size")]
        public string PageSize { get; set; }

        [JsonProperty(PropertyName = "products")]
        public IEnumerable<JsonProductDTO> Products { get; set; }
    }    
}
