﻿using Newtonsoft.Json;

namespace Beltis.Challenge.ProductScrapping.Domain.DTO
{
    public class JsonProductDTO
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "product_name")]
        public string ProductName { get; set; }

        [JsonProperty(PropertyName = "quantity")]
        public string Quantity { get; set; }

        [JsonProperty(PropertyName = "categories")]
        public string Categories { get; set; }

        [JsonProperty(PropertyName = "packaging")]
        public string Packaging { get; set; }

        [JsonProperty(PropertyName = "brands")]
        public string Brands { get; set; }

        [JsonProperty(PropertyName = "image_url")]
        public string ImageUrl { get; set; }
    }
}