﻿using Beltis.Challenge.ProductScrapping.Business.Services;
using Beltis.Challenge.ProductScrapping.Data.Repositories;
using Beltis.Challenge.ProductScrapping.Domain.Entities;
using Beltis.Challenge.ProductScrapping.Domain.Repositories;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using RichardSzalay.MockHttp;
using System.Reflection;

namespace Beltis.Challenge.ProductScrapping.Tests.Services
{
    public class ScrappringServiceTests : TestFixtures
    {
        [Fact]
        public async void ShouldScrapProductsFromOpenFoodFacts()
        {
            var databaseContext = GetDatabaseContext();

            var logger = Mock.Of<ILogger<ScrappringService>>();
            var productRepository = new ProductRepository(databaseContext);
            var product = GenerateProduct();

            var scrappringServiceMock = new Mock<ScrappringService>(productRepository, logger);
            scrappringServiceMock.CallBase = true;
            scrappringServiceMock.Setup(x => x.ScrapProductsFromPage()).ReturnsAsync(new List<Product> { product });
            var scrappringService = scrappringServiceMock.Object;

            await scrappringService.ScrapProductsFromOpenFoodFacts();

            var insertedProduct = databaseContext.Products.Where(x => x.Id == product.Id).Single();

            insertedProduct.Should().NotBeNull();
            insertedProduct.ProductName.Should().Be(product.ProductName);
            insertedProduct.Code.Should().Be(product.Code);
            insertedProduct.Brands.Should().Be(product.Brands);
        }

        [Fact]
        public async void ShouldScrapProductsFromPage()
        {
            string executingAssemblyDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string path = Path.Combine(executingAssemblyDirectory, "Files/food_facts.html");
            string pageHtml = File.ReadAllText(path);

            var logger = Mock.Of<ILogger<ScrappringService>>();
            var productRepository = Mock.Of<IProductRepository>();
            var scrappringServiceMock = new Mock<ScrappringService>(productRepository, logger);
            scrappringServiceMock.CallBase = true;
            scrappringServiceMock.Setup(x => x.FetchSiteHTML()).ReturnsAsync(pageHtml);
            scrappringServiceMock.Setup(x => x.FetchProducts(It.IsAny<IList<Product>>()))
                .ReturnsAsync(new List<Product> { new Product { Code = 3017620422003L, Barcode = "3017620422003 (EAN / EAN-13)" } });
            var scrappringService = scrappringServiceMock.Object;

            var result = await scrappringService.ScrapProductsFromPage();
            var product = result.FirstOrDefault();

            product?.Code.Should().Be(3017620422003);
            product?.Barcode.Should().Be("3017620422003 (EAN / EAN-13)");
        }

        [Fact]
        public async void ShouldFetchProducts()
        {
            var products = new List<Product> { GenerateProduct(), GenerateProduct() };

            var codes = string.Join(',', products.Select(x => x.Code));
            var url = $"https://world.openfoodfacts.org/api/v2/search.json?code={codes}&fields=code,product_name,quantity,categories,packaging,brands,image_url&page=";
            var response = "{\"count\":1,\"page\":1,\"page_count\":1,\"page_size\":1,\"products\":[{\"code\":\"" + products.First().Code.ToString() + "\",\"brands\":\"Courmayeur\",\"categories\":\"Boissons, Eaux, Eaux de sources, Eaux minérales, Eaux minérales naturelles, Boissons sans sucre ajouté\",\"image_url\":\"https://images.openfoodfacts.org/images/products/802/488/450/0403/front_fr.102.400.jpg\",\"packaging\":\"Plastique, Bouteille, Bouteille en plastique\",\"product_name\":\"Courmayeur - Eau minérale naturelle\",\"quantity\":\"1,5 L\"}],\"skip\":0}";
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(url + "1")
                    .Respond("application/json", response);
            mockHttp.When(url + "2")
                    .Respond("application/json", response);
            mockHttp.When(url + "3")
                    .Respond("application/json", response);

            var logger = Mock.Of<ILogger<ScrappringService>>();
            var productRepository = Mock.Of<IProductRepository>();
            var scrappringServiceMock = new Mock<ScrappringService>(productRepository, logger);
            scrappringServiceMock.CallBase = true;
            scrappringServiceMock.Setup(x => x.GetHttpClient()).Returns(mockHttp.ToHttpClient());
            var scrappringService = scrappringServiceMock.Object;

            var retiviedProducts = await scrappringService.FetchProducts(products);
            var product = retiviedProducts.ToList().FirstOrDefault();

            product.ProductName.Should().Be("Courmayeur - Eau minérale naturelle");
            product.Quantity.Should().Be("1,5 L");
            product.Categories.Should().Be("Boissons, Eaux, Eaux de sources, Eaux minérales, Eaux minérales naturelles, Boissons sans sucre ajouté");
            product.Packaging.Should().Be("Plastique, Bouteille, Bouteille en plastique");
            product.Brands.Should().Be("Courmayeur");
            product.ImageUrl.Should().Be("https://images.openfoodfacts.org/images/products/802/488/450/0403/front_fr.102.400.jpg");
        }

        [Fact]
        public async void ShouldFetchSiteHTML()
        {
            string executingAssemblyDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string path = Path.Combine(executingAssemblyDirectory, "Files/food_facts.html");
            string pageHtml = File.ReadAllText(path);

            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When("https://world.openfoodfacts.org/")
                    .Respond("text/html", pageHtml);

            var logger = Mock.Of<ILogger<ScrappringService>>();
            var productRepository = Mock.Of<IProductRepository>();
            var scrappringServiceMock = new Mock<ScrappringService>(productRepository, logger);
            scrappringServiceMock.CallBase = true;
            scrappringServiceMock.Setup(x => x.GetHttpClient()).Returns(mockHttp.ToHttpClient());
            var scrappringService = scrappringServiceMock.Object;

            var retiviedPageHtml = await scrappringService.FetchSiteHTML();

            retiviedPageHtml.Should().Be(pageHtml);
        }

        [Fact]
        public void ShouldExtractProductCodeFromHref()
        {
            var logger = Mock.Of<ILogger<ScrappringService>>();
            var productRepository = Mock.Of<IProductRepository>();
            var scrappringService = new ScrappringService(productRepository, logger);

            var productCode = scrappringService.ExtractProductCodeFromHref("/product/3017620422003/nutella-ferrero");
            
            productCode.Should().Be(3017620422003L);
        }

        [Fact]
        public void ShouldGetHttpClient()
        {
            var logger = Mock.Of<ILogger<ScrappringService>>();
            var productRepository = Mock.Of<IProductRepository>();
            var scrappringService = new ScrappringService(productRepository, logger);

            var client = scrappringService.GetHttpClient();

            client.Should().BeOfType<HttpClient>();
        }
    }
}
