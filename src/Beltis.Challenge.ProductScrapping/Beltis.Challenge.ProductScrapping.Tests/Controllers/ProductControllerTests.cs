﻿using Beltis.Challenge.ProductScrapping.Data.Context;
using Beltis.Challenge.ProductScrapping.Domain.Entities;
using Beltis.Challenge.ProductScrapping.Domain.ResponseObject;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.Net;

namespace Beltis.Challenge.ProductScrapping.Tests.Controllers
{
    public class ProductControllerTests : TestFixtures
    {
        [Fact]
        public async void ShouldReturnOK_BaseURL()
        {
            var result = await _httpClient.GetAsync("/");
            result.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = result.Content.ReadAsStringAsync();
            content.Result.Should().Be("Fullstack Challenge 20201026");
        }

        [Fact]
        public async void ShouldReturnOK_GetProduct()
        {
            // Arrange
            using var scope = _application.Services.CreateScope();
            var context = scope.ServiceProvider.GetService<ProductScrappingContext>();
            var product = GenerateProduct();
            await context.Products.AddAsync(product);
            await context.SaveChangesAsync();

            // Act
            var result = await _httpClient.GetAsync($"api/products/{product.Code}");

            // Test
            result.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = result.Content.ReadAsStringAsync();
            var retriviedProduct = JsonConvert.DeserializeObject<Product>(content.Result);
            retriviedProduct.Should().BeEquivalentTo(product);

            // Dispose
            context.Products.Remove(product);
            await context.SaveChangesAsync();
        }

        [Fact]
        public async void ShouldReturnNotFound_GetProduct()
        {
            // Arrange
            var product = GenerateProduct();

            // Act
            var result = await _httpClient.GetAsync($"api/products/{product.Code}");

            // Test
            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void ShouldReturnOK_GetPagedProducts()
        {
            // Arrange
            using var scope = _application.Services.CreateScope();
            var context = scope.ServiceProvider.GetService<ProductScrappingContext>();
            var product1 = GenerateProduct();
            var product2 = GenerateProduct();
            var product3 = GenerateProduct();
            var product4 = GenerateProduct();
            await context.Products.AddAsync(product1);
            await context.Products.AddAsync(product2);
            await context.Products.AddAsync(product3);
            await context.Products.AddAsync(product4);
            await context.SaveChangesAsync();

            // Act
            int page = 1;
            int pageSize = 10;
            var result = await _httpClient.GetAsync($"api/products?page={page}&pageSize={pageSize}");

            // Test
            result.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = result.Content.ReadAsStringAsync();
            var retriviedProduct = JsonConvert.DeserializeObject<PagedQueryResponseObject<Product>>(content.Result);
            retriviedProduct.Data.Single(p => p.Code == product1.Code).Should().BeEquivalentTo(product1);

            // Dispose
            context.Products.Remove(product1);
            context.Products.Remove(product2);
            context.Products.Remove(product3);
            context.Products.Remove(product4);
            await context.SaveChangesAsync();
        }

        [Fact]
        public async void ShouldReturnNotFound_GetPagedProducts()
        {
            // Act
            int page = 0;
            int pageSize = 10;
            var result = await _httpClient.GetAsync($"api/products?page={page}&pageSize={pageSize}");

            // Test
            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
    }
}
