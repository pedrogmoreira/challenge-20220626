﻿using Beltis.Challenge.ProductScrapping.Data.Context;
using Beltis.Challenge.ProductScrapping.Domain.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.CodeAnalysis;

namespace Beltis.Challenge.ProductScrapping.Tests
{
    [ExcludeFromCodeCoverage]
    public abstract class TestFixtures
    {
        protected readonly HttpClient _httpClient;
        protected readonly WebApplicationFactory<Program> _application;

        public TestFixtures()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Test");
            _application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder =>
                {
                    builder.UseEnvironment("Test");

                });
            _httpClient = _application.CreateClient();
        }

        protected ProductScrappingContext GetDatabaseContext()
        {
            var options = new DbContextOptionsBuilder<ProductScrappingContext>()
                .UseInMemoryDatabase("ProductScrapping.Test.Data")
                .Options;

            return new ProductScrappingContext(options);
        }

        protected Product GenerateProduct()
        {
            return new Product()
            {
                Id = Guid.NewGuid(),
                Code = Faker.RandomNumber.Next(10000000, 99999999),
                Barcode = Faker.RandomNumber.Next(10000000, 99999999).ToString("D9"),
                Brands = Faker.Company.Name(),
                Categories = string.Join(", ", Faker.Lorem.Words(3).ToArray()),
                ImportedT = Faker.DateOfBirth.Next(),
                Packaging = Faker.Lorem.Sentence(5),
                ProductName = Faker.Lorem.GetFirstWord(),
                Quantity = Faker.RandomNumber.Next(999).ToString("#g"),
                Status = Status.IMPORTED,
                ImageUrl = Faker.Internet.DomainName(),
                Url = Faker.Internet.DomainName()
            };
        }
    }
}
