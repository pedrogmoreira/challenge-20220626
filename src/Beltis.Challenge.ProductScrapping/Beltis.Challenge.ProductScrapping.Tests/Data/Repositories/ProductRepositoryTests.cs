using Beltis.Challenge.ProductScrapping.Data.Repositories;
using FluentAssertions;

namespace Beltis.Challenge.ProductScrapping.Tests.Data.Repositories
{
    public class ProductRepositoryTests : TestFixtures
    {
        [Fact]
        public async void ShouldReturnAllProducts()
        {
            var context = GetDatabaseContext();

            var product1 = GenerateProduct();
            var product2 = GenerateProduct();
            context.Products.Add(product1);
            context.Products.Add(product2);
            await context.SaveChangesAsync();

            var productRepository = new ProductRepository(context);
            var products = productRepository.GetAll();

            products.Should().HaveCount(2);
            products.Single(x => x.Id == product1.Id).Should().BeEquivalentTo(product1);

            await context.Database.EnsureDeletedAsync();
        }

        [Fact]
        public async void ShouldReturnPagedProducts()
        {
            var context = GetDatabaseContext();

            var product1 = GenerateProduct();
            var product2 = GenerateProduct();
            var product3 = GenerateProduct();
            context.Products.Add(product1);
            context.Products.Add(product2);
            context.Products.Add(product3);
            await context.SaveChangesAsync();

            var productRepository = new ProductRepository(context);
            var products = productRepository.GetPaged(1, 2);

            products.Data.Should().HaveCountGreaterThan(0);
            products.Data.Should().HaveCountLessThanOrEqualTo(2);
            products.Data.Single(x => x.Id == product1.Id).Should().BeEquivalentTo(product1);

            await context.Database.EnsureDeletedAsync();
        }

        [Fact]
        public async void ShouldReturnEmptyPagedProductsAsync()
        {
            var context = GetDatabaseContext();

            var product1 = GenerateProduct();
            var product2 = GenerateProduct();
            var product3 = GenerateProduct();
            context.Products.Add(product1);
            context.Products.Add(product2);
            context.Products.Add(product3);
            await context.SaveChangesAsync();

            var productRepository = new ProductRepository(context);
            var products = productRepository.GetPaged(0, 2);

            products.Data.Should().HaveCount(0);

            await context.Database.EnsureDeletedAsync();
        }

        [Fact]
        public async void ShouldReturnFindProductAsync()
        {
            var context = GetDatabaseContext();

            var product1 = GenerateProduct();
            var product2 = GenerateProduct();
            var product3 = GenerateProduct();
            context.Products.Add(product1);
            context.Products.Add(product2);
            context.Products.Add(product3);
            await context.SaveChangesAsync();

            var productRepository = new ProductRepository(context);
            var productFound = await productRepository.Find(p => p.Id == product2.Id);

            productFound.Should().NotBeNull();
            productFound.Should().BeEquivalentTo(product2);

            await context.Database.EnsureDeletedAsync();
        }

        [Fact]
        public async void ShouldReturnInsertProduct()
        {
            var context = GetDatabaseContext();

            var productRepository = new ProductRepository(context);
            var product = GenerateProduct();
            await productRepository.Insert(product);

            var productInserted = context.Products.FirstOrDefault();
            productInserted.Should().NotBeNull();
            productInserted.Should().BeEquivalentTo(product);
        }
    }
}
